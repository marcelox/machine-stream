# Instructions
Please install npm (node version >12 / npm version > 6.14)
```bash
npm install
```

For local development, please run
```bash
npm run dev
```
To build for production:
```bash
npm run build
```


