import { useState, useEffect } from 'react';
import ReactDom from 'react-dom';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';
import Machine from './Machine';
import Events from './Events';

const App = () => {
  const [machine, setMachine] = useState([]);

  useEffect(() => {
    request_machines();
  }, []);

  async function request_machines() {
    const response = await fetch(
      `https://machinestream.herokuapp.com/api/v1/machines`
    );

    const json = await response.json();
    setMachine(json.data);
  }

  return (
    <>
      <Router>
        <header>
          <Events />
          <Link className="home-link" to="/">
            <h1>ZEISS MachineStream</h1>
          </Link>
          <h2>An overview of microscopes and measurement machines</h2>
        </header>
        <main>
          <Switch>
            <Route path="/events">
              <Events />
            </Route>
            <Route path="/machine-details/id=:id">
              <Machine />
            </Route>
            <Route path="/">
              <ul className="main-list">
                {machine.map((machine) => (
                  <li key={machine.id}>
                    <span className="machine-type">
                      <span className="meta-info">Machine type</span>
                      {machine.machine_type}

                      <span className="machine-id">
                        <span className="meta-info">Machine Id</span>
                        <span>{machine.id}</span>
                      </span>
                    </span>

                    <span className="status">
                      <span className="meta-info">status</span>
                      <span>{machine.status}</span>
                    </span>
                    <Link
                      className="details"
                      to={{
                        pathname: `/machine-details/id=${machine.id}`
                      }}
                    >
                      Details
                    </Link>
                  </li>
                ))}
              </ul>
            </Route>
          </Switch>
        </main>
      </Router>
    </>
  );
};

ReactDom.render(<App />, document.querySelector('#root'));
