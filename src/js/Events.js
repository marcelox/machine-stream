import { useEffect, useState } from 'react';
import { Socket } from 'phoenix';

const Events = () => {
  const [event, setEvent] = useState('idle');

  useEffect(() => {
    const socket = new Socket('ws://machinestream.herokuapp.com/api/v1/events');
    socket.connect();
    const channel = socket.channel('events', { client: 'browser' });
    channel.join();
    channel.on('new', (event) => setEvent(event));
  }, [event]);
  return (
    <div className="live-updates">
      <p className="live-updates-title">
        Live updates | Event stream of machine status updates
      </p>
      <ul className="live-updates-list">
        <li>
          <span className="live-updates-meta">Machine Id</span>
          {event.machine_id}
        </li>
        <li>
          <span className="live-updates-meta">Status</span>
          {event.status}
        </li>
        <li>
          <span className="live-updates-meta">Timestamp</span>
          {event.timestamp}
        </li>
      </ul>
    </div>
  );
};

export default Events;
