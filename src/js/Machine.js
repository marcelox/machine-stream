import { useState, useEffect } from 'react';
import { withRouter, useParams } from 'react-router-dom';

const Machine = () => {
  const [details, setDetails] = useState([]);
  const { id } = useParams();

  useEffect(() => {
    request_machine_details();
  });

  async function request_machine_details() {
    const response = await fetch(
      `https://machinestream.herokuapp.com/api/v1/machines/${id}`
    );

    const json = await response.json();
    setDetails(json.data);
  }
  return (
    <div className="machine-details">
      <h3 className="machine-details-id">
        <span>Machine Id</span>
        {id}
      </h3>
      <p>
        <span>Machine Type</span>
        {details.machine_type}
      </p>
      <p>
        <span>Status</span>
        {details.status}
      </p>
      <p>
        <span>Floor</span>
        {details.floor}
      </p>
      <p>
        <span>Last Maintenance</span>
        {details.last_maintenance}
      </p>
      <p>
        <span>Install date</span>
        {details.install_date}
      </p>
    </div>
  );
};

export default withRouter(Machine);
